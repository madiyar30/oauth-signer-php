<?php
require __DIR__ . '/vendor/autoload.php';
use Mastercard\Developer\OAuth\Utils\AuthenticationUtils;
use Mastercard\Developer\OAuth\OAuth;

$path = __DIR__.'/resources/certificate.pfx';
$password = 'c001988835';
$consumerKey = '22c5bb6af47f4834afca12c269d194b9';
$signingKey =  AuthenticationUtils::loadSigningKey(
    $path,
    '',
    $password);

$uri = 'https://testwallet.masterpass.ru/mpapiv2/MerchantStart';
//$uri = 'https://google.com';
$method = 'POST';
$params = [
    'Channel' => 1,
    'Fingerprint' => 'RGV2aWNlSWQ9OTNlNmVkNjAtMzlkNi04ZWM2LTI5YWItZGUxMmM0NWI4NWM3fHx8dXNlcl9hZ2VudD1Nb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvODkuMC40Mzg5LjgyIFNhZmFyaS81MzcuMzZ8fHxsYW5ndWFnZT1ydS1SVXx8fGNvbG9yX2RlcHRoPTI0fHx8cGl4ZWxfcmF0aW89MXx8fGhhcmR3YXJlX2NvbmN1cnJlbmN5PTEyfHx8cmVzb2x1dGlvbj0xOTIwLDEwODB8fHxhdmFpbGFibGVfcmVzb2x1dGlvbj0xOTIwLDEwNDB8fHx0aW1lem9uZV9vZmZzZXQ9LTM2MHx8fHNlc3Npb25fc3RvcmFnZT0xfHx8bG9jYWxfc3RvcmFnZT0xfHx8aW5kZXhlZF9kYj0xfHx8b3Blbl9kYXRhYmFzZT0xfHx8Y3B1X2NsYXNzPXVua25vd258fHxuYXZpZ2F0b3JfcGxhdGZvcm09V2luMzJ8fHxkb19ub3RfdHJhY2s9dW5rbm93bnx8fHJlZ3VsYXJfcGx1Z2lucz0yODUyMTI5NTJ8fHxjYW52YXM9LTE0MjQzMzczNDZ8fHx3ZWJnbD0xODk5MTUzNDMyfHx8YWRibG9jaz1mYWxzZXx8fGhhc19saWVkX2xhbmd1YWdlcz1mYWxzZXx8fGhhc19saWVkX3Jlc29sdXRpb249ZmFsc2V8fHxoYXNfbGllZF9vcz1mYWxzZXx8fGhhc19saWVkX2Jyb3dzZXI9ZmFsc2V8fHx0b3VjaF9zdXBwb3J0PTAsZmFsc2UsZmFsc2V8fHxqc19mb250cz0tMTk0NzQyMjg5NQ==',
    'MerchantName' => 'Madiyar',
    'Phone' => '77027124813',
    'PhoneCheckDate' => round(microtime(true) * 1000)
];
ksort($params);
$payload = http_build_query($params);
$headers = array(
//    'Content-Type: application/x-www-form-urlencoded'
//    'Content-Length: ' . strlen($payload)
);
$handle = curl_init($uri);
//curl_setopt($handle, CURLOPT_POST, 0);
curl_setopt_array($handle, array(
//        CURLINFO_HEADER_OUT => 1,
        CURLOPT_POST, 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POSTFIELDS => $payload
    )
);
$authHeader = OAuth::getAuthorizationHeader($uri, $method, $payload, $consumerKey, $signingKey);
$headers[] = OAuth::AUTHORIZATION_HEADER_NAME . ': ' . $authHeader;
curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
curl_setopt($handle, CURLINFO_HEADER_OUT, true);
curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);


//var_dump(json_encode(curl_getinfo($handle)));

$result = curl_exec($handle); // There is no way of reading HTTP headers without having the request successfully sent
if (curl_errno($handle)) {
    echo 'Error:' . curl_error($handle);
}
curl_close($handle);
var_dump($result);
?>
<html>
<head>
    <title>test</title>
    <script src="MPWalletFingerprint.min.js"></script>
</head>
<body>
<br><br>
<p id="h1"></p><br>
<br>

<script>
    function some() {
        MPWalletFingerprint.getFingerprint('https://testwallet.masterpass.ru/').then(
            // Получение и обработка ответа
            function(result){
                // Обработка успешного ответа
                console.log(result);
                document.getElementById('h1').innerHTML = result
                console.log(hashCode(result));
                // result - закодированная base64 строка с фингерпринтом для использования в методе Login
            },
            function(err){
                // Обработка неуспешного ответа
                console.log(err);
            }
        );
    }

    function hashCode(string) {
        var hash = 0, i, chr;
        if (string.length === 0) return hash;
        for (i = 0; i < string.length; i++) {
            chr = string.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }
    some()
</script>
</body>
</html>
